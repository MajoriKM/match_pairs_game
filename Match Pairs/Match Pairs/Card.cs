﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Audio;
using System.IO;

namespace Match_Pairs
{
    class Card
    {

        // type definition

        public enum symbol // special type of integer with specific values allowed that have names
        {
            CHICKEN, // =0
            HORSE, // = 1
            ELEPHANT, // = 2
        }

        //Data

        Texture2D card = null;
        Texture2D chick = null;
        Vector2 position = Vector2.Zero;
        bool flipped = false;

        SoundEffect clickSFX;
        symbol oursymbol = symbol.CHICKEN;

        //Behaviour


        
        public void LoadContent(ContentManager content)
        {

            card = content.Load<Texture2D>("Graphics/card");
            chick = content.Load<Texture2D>("graphics/chicken");

            clickSFX = content.Load<SoundEffect>("Audio/buttonclick");
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(card, position, Color.White);
            if(flipped)
                spriteBatch.Draw(chick, position, Color.White);
        }

        public void Cardplacement(GameWindow window)
        {
            //int positionYMin = 0;
            //int positionXMin = 0;
            int positionYMax = window.ClientBounds.Height - card.Height;
            int positionXMax = window.ClientBounds.Width - card.Width;

            position.X = positionXMax - 100;
            position.Y = positionYMax -100;
        }

        public void Input()
        {
            MouseState currentState = Mouse.GetState();

            Rectangle cardBounds = new Rectangle((int)position.X,
                (int)position.Y,
                card.Width,
                card.Height);

            if (currentState.LeftButton == ButtonState.Pressed
                && cardBounds.Contains(currentState.X, currentState.Y))
            {
                flipped = true;

                clickSFX.Play();
            }
        }
    }
}
